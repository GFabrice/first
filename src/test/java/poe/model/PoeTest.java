package poe.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class PoeTest {

    @Test
    void testDefaultParameter() {
        Poe poe = new Poe();

        // Tester les attributes vides
        assertNull(poe.getTitle());
        assertNull(poe.getBeginDate());
        assertNull(poe.getEndDate());
        assertNull(poe.getPoeType());

        poe.setTitle("JAVA Angular fullstack");
        System.out.println(poe.getTitle());

        LocalDate beginDate = LocalDate.of(2022, 11, 01);
        poe.setBeginDate(beginDate);
        System.out.println(poe.getBeginDate());

        LocalDate endDate = LocalDate.of(2023, 1, 15);
        poe.setEndDate(endDate);
        System.out.println(poe.getEndDate());

    }

    @Test
    void testAllParameters() {

    }

    @Test
    void testToString() {
    }
}